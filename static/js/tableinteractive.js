(async () => {
// "await" permet au JS de terminer de charger la route avant de passer à la 
// ligne suivante.
// axios.get permet de recupérer une donnée.
    const json = await axios.get('/datatable');
    const jsonn = json.data;

// Console.log permet de voir dans l'inspecteur de la page web si la donn�e est
// correctement r�cup�r�e.
    console.log(json)

// Boucle qui it�re sur les donn�es du Json
    for (let i = 0; i < jsonn.length; i++) {

        var dataSet = jsonn[i].data;

        $(document).ready(function () {
            $('#tableJS').DataTable({
                data: dataSet,
                columns: [
                    {title: "Aliment"},
                    {title: "Eau g"},
                    {title: "Vitamine K µg"},
                    {title: "Sodium mg"},
                    {title: "Cuivre mg"},
                    {title: "Protéines g"},
                    {title: "Bêta-carotène µg"},
                    {title: "Magnésium mg"},
                    {title: "Phosphore mg"},
                    {title: "Calories kcal"},
                    {title: "Vitamine C mg"},
                    {title: "Potassium mg"},
                    {title: "Vitamine B2 mg"},
                    {title: "Vitamine B1 mg"},
                    {title: "Manganèse mg"},
                    {title: "Zinc mg"},
                    {title: "Vitamine E mg"},
                    {title: "Vitamine A mg"},
                    {title: "Fibres g"},
                    {title: "Vitamine B9 µg"},
                    {title: "Lipides g"},
                    {title: "Vitamine B3 mg"},
                    {title: "Vitamine B6 mg"},
                    {title: "Calcium mg"},
                    {title: "Glucides g"},
                    {title: "Vitamine B5 mg"},
                    {title: "Energie kJ"},
                    {title: "Fer mg"},
                    {title: "Fibres alimentaires g"}
                ],
                scrollX: true,
                scrollCollapse: true,
            });


        });
    }
})()