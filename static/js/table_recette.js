(async () => {

    // "await" permet au JS de terminer de charger la route avant de passer à 
    // la ligne suivante.
    // axios.get permet de recupérer une donnée.
    const json = await axios.get('/datatablerecette');
    const jsonn = json.data;

    // Console.log permet de voir dans l'inspecteur de la page web si la donnée
    // est correctement récupérée.
    console.log(json)

    // Boucle qui itère sur les données du Json
    for (let i = 0; i < jsonn.length; i++) {

        google.charts.load('current', {'packages': ['table']});
        google.charts.setOnLoadCallback(drawTable);

        function drawTable() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Valeurs nutritionnelles');
            data.addColumn('number', 'Quantitees');

            data.addRows(jsonn[i].data);

            var table = new google.visualization.Table(document.getElementById('table_div'));

            table.draw(data, {
                showRowNumber: true,
                width: '100%',
                height: '100%'
            });

            var table = new google.visualization.ChartWrapper({
                chartType: 'Table',
                containerId: 'table_div',
                options: {
                    cssClassNames: cssClassNames,
                    allowHtml: true
                }
            });

        }
    }
})()