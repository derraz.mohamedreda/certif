(async () => {

// "await" permet au JS de terminer de charger la route avant de passer à la 
// ligne suivante.
// axios.get permet de recupérer une donnée.
    const json = await axios.get('/datasliced_micro');
    const jsonn = json.data;

// Console.log permet de voir dans l'inspecteur de la page web si la donnée est
// correctement récupérée.
    console.log(json)

// Boucle qui itère sur les données du Json
    for (let i = 0; i < jsonn.length; i++) {

// Créer les div sur le html, creer l'id en fonction de l'indice i, puis les
// places l'une en dessous de l'autre.
        let newDiv = document.createElement('div');
        newDiv.id = jsonn[i].table;
        document.body.appendChild(newDiv);

// Theme d'animation
        am4core.useTheme(am4themes_animated);

// Vecteur d'image, pris dans une image en SVG
        var iconPath = "M18,20c-0.6,0-1-0.4-1-1s0.4-1,1-1h3.6c0.3-1.8,0.4-3.3,0.4-3.8c0-1-0.2-1.9-0.6-2.7c-0.4-0.7-0.9-1.4-1.5-1.9  c0.8-0.6,1.4-1.4,1.9-2.3C22,7,22,6.7,21.9,6.4c0-0.1-0.1-0.2-0.1-0.3c-0.3-0.7-0.9-1.2-1.6-1.4c-0.5-0.2-1.1-0.2-1.7,0  C18.2,3.1,17.2,2,16,2c-1.2,0-2.2,1.1-2.6,2.7c-0.6-0.1-1.1-0.1-1.6,0.1c-0.7,0.2-1.2,0.7-1.5,1.4c-0.1,0.2-0.1,0.4-0.2,0.6  c-0.1,0.3,0,0.5,0.1,0.7C10.7,8.3,11.3,9,12,9.6c-1.2,1.1-2,2.7-2,4.4h5c0.6,0,1,0.4,1,1s-0.4,1-1,1h-4.8c0.2,1.5,0.5,3.8,1.1,6H16  c0.6,0,1,0.4,1,1s-0.4,1-1,1h-4.2c1,3.2,2.4,6,4.2,6c2.6,0,4.3-5.5,5.2-10H18z M16.1,9C16,9,16,9,16,9c-1.5,0-2.8-0.8-3.8-2.2  c0.1-0.1,0.1-0.1,0.2-0.1c0.3-0.1,0.8,0,1.3,0.3c0.3,0.2,0.7,0.2,1,0c0.3-0.2,0.5-0.5,0.5-0.8C15.2,4.7,15.8,4,16,4  c0.1,0,0.8,0.7,0.8,2.2c0,0.4,0.2,0.7,0.5,0.8c0.3,0.2,0.7,0.1,1,0c0.6-0.4,1.1-0.4,1.3-0.3c0.1,0,0.1,0,0.2,0.1  c-0.5,0.8-1.2,1.4-1.9,1.8c-0.2,0-0.4,0.1-0.5,0.2C17,8.9,16.5,9,16.1,9z"

        var chart = am4core.create(jsonn[i].table, am4charts.SlicedChart);
        chart.hiddenState.properties.opacity = 0;
        chart.paddingLeft = 50;

        chart.data = jsonn[i].data;

        var series = chart.series.push(new am4charts.PictorialStackedSeries());
        series.dataFields.value = "valeur_chiffre";
        series.dataFields.category = "valeur";
        series.alignLabels = true;

        var label = chart.createChild(am4core.Label);
        label.text = jsonn[i].label;
        label.fontSize = 50;
        label.align = "center";

        series.maskSprite.path = iconPath;
        series.ticks.template.locationX = 1;
        series.ticks.template.locationY = 0;

        series.labelsContainer.width = 200;
    }
})()