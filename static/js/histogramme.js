(async () => {

// "await" permet au JS de terminer de charger la route avant de passer à la 
// ligne suivante.
// axios.get permet de recupérer une donnée.
    const json = await axios.get('/datahistogrammes');
    const jsonn = json.data;

// Console.log permet de voir dans l'inspecteur de la page web si la donnée est
// correctement récupérée.
    console.log(json)

// Boucle qui itère sur les données du Json
    for (let i = 0; i < jsonn.length; i++) {

// Créer les div sur le html, creer l'id en fonction de l'indice i, puis les
// places l'une en dessous de l'autre.
        let newDiv = document.createElement('div');
        newDiv.id = jsonn[i].table;
        document.body.appendChild(newDiv);

// Theme d'animation
        am4core.useTheme(am4themes_animated);

// Creation du graphique
        var chart = am4core.create(jsonn[i].table, am4charts.XYChart);
        chart.scrollbarX = new am4core.Scrollbar();

// Ajout de la donnée
        chart.data = jsonn[i].data;

// Creation des axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "aliment";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = 270;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;

        var label = chart.createChild(am4core.Label);
        label.text = jsonn[i].label;
        label.fontSize = 50;
        label.align = "center";

// Creation d'une serie
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "valeur_chiffre";
        series.dataFields.categoryX = "aliment";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;
        series.tooltip.pointerOrientation = "vertical";
        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;

// Arrondis les angles
        var hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;

        series.columns.template.adapter.add("fill", function (fill, target) {
            return chart.colors.getIndex(target.dataItem.index);
        });

        chart.cursor = new am4charts.XYCursor();
    }

})()