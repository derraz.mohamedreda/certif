# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 21:25:48 2020

Complète la page Flaskdigidashboard.py, c'est ici que les classes sont créées,
c'est le wrapper du module DigiDashBoard.py.

L'utilisateur n'aura qu'à les appeler sur le code Utilisateur et y mettre ses
données et sa personalisation pour avoir son dashboard.

@author: Reda
"""

import Flaskdigidashboard as fdb


# CLasse pour les boutons, sous-titres et graphiques du tableau de bord


class Boutonstitres:
    boutonstitres = {}

    def __init__(self, boutonstitres):
        self.boutonstitres = boutonstitres

    def initialisation(self):
        fdb.boutonstitres = self.boutonstitres


class Graphiques:
    graphiques = {}

    def __init__(self, graphiques):
        self.graphiques = graphiques

    def initialisation(self):
        fdb.graphiques = self.graphiques


class Fonctionnalites:
    fonctionnalites = {}

    def __init__(self, fonctionnalites):
        self.fonctionnalites = fonctionnalites

    def initialisation(self):
        fdb.fonctionnalites = self.fonctionnalites


if __name__ == '__main__':
    fdb.app.run(debug=False, use_reloader=False)
