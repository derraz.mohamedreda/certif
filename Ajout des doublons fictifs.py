# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 10:52:21 2020

Ajout de doublons fictif

@author: Reda
"""

import pandas as pd
import psycopg2
from sqlalchemy import create_engine

engine = create_engine(
    'postgresql://postgres:digifab@localhost:5432/ProjetPerso')

con = psycopg2.connect(host="localhost",
                       database="ProjetPerso",
                       user="postgres",
                       password="digifab",
                       port=5432)

sql = "select * from table_aliments_doublons;"
table_aliment_doublons = pd.read_sql_query(sql, con)
cur = con.cursor()

# Copie de la table initiale et crééer une table où je vais inserer un doublon.
table_aliment_doublons.to_sql('table_aliments_doublons', engine, index=True)

# J'insère le doublon dans la table.
requete = "INSERT INTO table_aliments_doublons ('Nom') VALUES ('Abricot')"
cur.execute(requete)
con.commit()

# Masque qui permet de boléaniser les doublons, "first" permet de garder
# la ligne initiale.
mask = table_aliment_doublons.nom.duplicated(keep="first")

# Dataframe des doublons qui sera créée en table.
table_liste_doublons = table_aliment_doublons[mask]

table_liste_doublons.to_sql("liste_doublons", engine,
                            if_exists="replace")

# Supprime les doublons de la table initiale.
table_aliment_doublons = table_aliment_doublons.drop_duplicates(subset=['nom'],
                                                                keep=False)
