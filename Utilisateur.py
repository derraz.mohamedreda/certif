# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 21:25:48 2020

Code principal de l'utilisateur, il n'a qu'appeler les classes initialisées
dans le code DigiDashBoard.py pour créer son dashboard.

Ne pas oublier d'importer DigiDashBoard et Flaskdigidashboard.

TODO : - Optimiser tout le code (avoir qu'un seul JSon et un seul HTML).
       - Pousser les introductions des codes Digidashboard.py et
        Flaskdigidashboard.py (expliquer le projet complet dans l'intro du code
                               Utilisateur.py)

@author: Reda
"""

import DigiDashBoard as DDB
import Flaskdigidashboard as fdb

graphiques = {
    "graphique1": "Histogramme",
    "graphique2": "Sliced",
    "graphique5": "Tableau"
}

boutonstitres = {
    "namedb1": "Digi",
    "namedb2": "DashBoard",
    "bouton1": "Accueil",
    "soustitre1": "Graphique",
    "soustitre2": "Nutrition",
    "soustitre3": "Documentation",
    "soustitre4": "Compte"
}

fonctionnalites = {
    "fonct1": "Recommandations",
    "fonct2": "Recettes"
}

boutonstitres = DDB.Boutonstitres(boutonstitres)
graphiques = DDB.Graphiques(graphiques)
fonctionnalites = DDB.Fonctionnalites(fonctionnalites)

boutonstitres.initialisation()
graphiques.initialisation()
fonctionnalites.initialisation()

if __name__ == '__main__':
    fdb.app.run(debug=True, use_reloader=False)
