# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 10:25:29 2020

L'utilisateur à la possibilité de connaitre l'ensemble des valeurs 
nutrionnelles ainsi que leur quantitée dans son repas, il lui suffit de 
renseigné les aliments de sa recette.

@author: Reda
"""

import math

import pandas as pd
import psycopg2

con = psycopg2.connect(host="localhost",
                       database="ProjetPerso",
                       user="postgres",
                       password="digifab",
                       port=5432)

sql = "select * from table_aliments3;"
tablealiments = pd.read_sql_query(sql, con)

liste_aliments = tablealiments.index.tolist()
liste_valeurs = tablealiments.columns.tolist()

# Il faut changer toutes les valeurs NaN en 0 pour pouvoir faire ma condition
# plus simplement.
for valeurs in tablealiments.columns:
    for aliment in liste_aliments:
        if math.isnan(tablealiments.loc[aliment][valeurs]) is True:
            tablealiments.loc[aliment][valeurs] = 0.0001

# Les valeurs des aliments sont au 100 grammes, le calcul suivant permet
# d'avoir sa valeur au gramme.
for aliment in liste_aliments:
    for valeur in liste_valeurs:
        if tablealiments.loc[aliment][valeur] != 0.0001:
            valeur_divise = tablealiments.loc[aliment][valeur] / 100
            tablealiments.loc[aliment][valeur] = valeur_divise


# Fonction qui retourne la quantité des valeurs nutritionnelles de la quantité
# de chaque aliment renseigné.
# Puis les additionnes pour obtenir un total de tous les apports nutritionnels
# de la recette
def recette(recette):
    # 'tous_les_aliments' est un dictionnaire contenant pour clé l'aliment, et
    # comme valeur le dictionnaire toutes_les_valeurs_et_quantitees.
    # 'toutes_les_valeurs_et_quantitees' est un dictionnaire co
    tous_les_aliments = {}
    toutes_les_valeurs_et_quantitees = {}
    for aliment, quantite in recette.items():
        for valeur_nutri in liste_valeurs:
            # Si l'aliment n'est pas égale à 0.0001 (qui est en fait un nan)
            if tablealiments.loc[aliment][valeur_nutri] != 0.0001:
                # Multiplie la valeur de la vauleur_nutri de l'aliment par sa
                # quantite
                quantite_valeur = (tablealiments.loc[aliment][valeur_nutri]
                                   * quantite)
                # Ajoute dans le dictionnaire
                # 'toutes_les_valeurs_et_quantitees' la quantité de la valeur
                # nutritionnelle de l'aliment renseigné par l'utilisateur
                toutes_les_valeurs_et_quantitees[valeur_nutri] = (
                    quantite_valeur)

        # Place le dictionnaire 'toutes_les_valeurs_et_quantitees' dans
        # la valeur aliment du dictionnaire tous_les_aliments
        tous_les_aliments[aliment] = toutes_les_valeurs_et_quantitees

        # Reinitisalise le dictionnaire pour reset les valeurs déjà présentes
        toutes_les_valeurs_et_quantitees = {}

    # Additionne toutes les valeurs d'une même valeur nutrionnelle des aliments
    # renseignés. Pour avoir un total des apports nutritionnelles dans une 
    # recette
    dictionnaire_total_valeur = {}

    for aliment in tous_les_aliments:
        # print(aliment)
        for key, value in tous_les_aliments[aliment].items():
            # print(key, value)
            if key in dictionnaire_total_valeur.keys():
                dictionnaire_total_valeur[key] = (dictionnaire_total_valeur
                                                  [key] + value)
            else:
                dictionnaire_total_valeur[key] = value

    # Visualisation sous forme de tableau plutot que de dictionnaire
    dictionnaire_total_valeur

    return dictionnaire_total_valeur


liste_des_aliments_quantitees = {'Courgette': 80, 'Carotte': 40, 'Avocat': 50,
                                 'Radis': 10}

recette(liste_des_aliments_quantitees)
