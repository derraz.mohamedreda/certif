# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 18:52:13 2020

Structure de la base de données complétement refaite.

Le remplissage de la nouvelle base de données se fera dans ce code.

@author: Reda
"""

import pandas as pd
import psycopg2

con = psycopg2.connect(host="localhost",
                       database="BDD CERTIF",
                       user="postgres",
                       password="digifab",
                       port=5432)
cur = con.cursor()

tablealiments = pd.read_excel('tableau_des_aliments9.xlsx', index_col=0)

# # Modification de l'unité de mesure pour eviter les problèmes d'encodages
# tablealiments = tablealiments.rename(columns={'vitamine_k_µg': 
#                                               'vitamine_k_mcg'})

# tablealiments = tablealiments.rename(columns={'beta_carotene_µg': 
#                                               'beta_carotene_mcg'})

# tablealiments = tablealiments.rename(columns={'vitamine_b9_µg': 
#                                               'vitamine_b9_mcg'})

# tablealiments.to_excel("tableau_des_aliments9.xlsx")

#
# REMPLISSAGE DE LA TABLE ALIMENT
#

# Liste des aliments
liste_aliments = []

for aliment in tablealiments.index:
    liste_aliments.append(aliment)

# Un while a été utilisé pour remplir la BDD lors du test de performance.

for aliment in liste_aliments:
    requete = "INSERT INTO aliment (nom_aliment) VALUES ("
    requete += "'" + str(aliment) + "'" + ")"
    cur.execute(requete)
    con.commit()

# 
# REMPLISSAGE DE LA TABLE APPORT
#

# Liste des apports
liste_apports = []

for apport in tablealiments.columns:
    liste_apports.append(apport)

for apport in liste_apports:
    requete = "INSERT INTO apport (nom_apport) VALUES ("
    requete += "'" + str(apport) + "'" + ")"
    cur.execute(requete)
    con.commit()

# 
# REMPLISSAGE DE LA TABLE CONTENIR
#

sql = "select * from apport"
table_apport = pd.read_sql_query(sql, con)

sql1 = "select * from aliment"
table_aliment = pd.read_sql_query(sql1, con)

dictionnaire_apport = table_apport.to_dict("records")
dictionnaire_aliment = table_aliment.to_dict("records")
dictionnaire_tablealiments = tablealiments.to_dict("records")

# Remplissage des colonnes id_aliment et id_apport.
for dict_aliment in dictionnaire_aliment:
    for dict_apport in dictionnaire_apport:
        id_aliment = dict_aliment['id_aliment']
        id_apport = dict_apport['id_apport']
        requete = "INSERT INTO contenir (id_aliment, id_apport) VALUES ("
        requete += str(id_aliment) + ", " + str(id_apport)
        requete += ")"
        cur.execute(requete)
        con.commit()

# Remplissage de la colonne apport_quantite.
x = 1
y = 1
for dictionnaire in dictionnaire_tablealiments:
    for apport, quantite in dictionnaire.items():
        requete = "UPDATE contenir SET apport_quantite = " + str(quantite)
        requete += " WHERE id_aliment =" + str(x) + "and id_apport =" + str(y)
        cur.execute(requete)
        con.commit()
        y += 1

    x += 1
    y = 1
#
# REMPLISSAGE DE LA TABLE ROLE
#

requete = "INSERT INTO role (id_role, nom_role) VALUES (0, 'administateur')"
cur.execute(requete)
con.commit()
requete = "INSERT INTO role (id_role, nom_role) VALUES (1, 'utilisateur')"
cur.execute(requete)
con.commit()

#
# AVOIR LA TABLE ALIMENT COMPLETTE EN DATAFRAME
#

sql2 = "SELECT aliment.nom_aliment, apport.nom_apport, contenir.apport_quantite"
sql2 += " FROM contenir"
sql2 += " JOIN aliment ON aliment.id_aliment = contenir.id_aliment"
sql2 += " JOIN apport ON apport.id_apport = contenir.id_apport"

table_aliment_complete = pd.read_sql_query(sql2, con)

table_aliment_complete.to_excel("tableau_des_aliments10.xlsx")
