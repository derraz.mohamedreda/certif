# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 13:46:36 2020

Test de performance entre l'écriture d'une base de donnée en sql, et avec
sqlalchemy.

@author: Reda
"""

import time

import matplotlib.pyplot as plt
import pandas as pd
import psycopg2
from sqlalchemy import create_engine

# =============================================================================
#                 PREPARATION D'UNE TABLE TEST POUR L'OPTIMISATION
# =============================================================================
engine = create_engine(
    'postgresql://postgres:digifab@localhost:5432/ProjetPerso')

con = psycopg2.connect(host="localhost",
                       database="ProjetPerso",
                       user="postgres",
                       password="digifab",
                       port=5432)

sql = "select * from table_aliments3;"
sql1 = "select * from liste_doublons"
tablealiments = pd.read_sql_query(sql, con)
df_colonne_minuscule = pd.read_sql_query(sql1, con)
cur = con.cursor()

table_optimisation = tablealiments

# Remplace les colonnes majuscule en minuscule
liste_colonne_vide = df_colonne_minuscule.columns.tolist()
liste_colonne_vide.remove('level_0')
liste_colonne_vide.remove('index')
liste_colonne_maj = table_optimisation.columns.tolist()

for maj, minu in zip(liste_colonne_maj, liste_colonne_vide):
    print(minu)
    table_optimisation = table_optimisation.rename(columns={maj: minu})

# Une majuscule n'était pas prise en compte (vitamine_C_mg)
table_optimisation = table_optimisation.rename(columns={'vitamine_C_mg':
                                                            'vitamine_c_mg'})

# Remplace toute les lettre majuscules en minuscules.
table_optimisation = table_optimisation.apply(
    lambda x: x.astype(str).str.lower())

dictionnaire_table = table_optimisation.to_dict("records")

liste_colonne = table_optimisation.columns.tolist()

# =============================================================================
#                               TEST DE PERFORMANCE
# =============================================================================

debut = time.time()
# Création de la table
requete = "CREATE TABLE public.table_avec_requetes"
requete += "("
requete += "   nom text,"
requete += "   eau_g text,"
requete += "   vitamine_k_µg text,"
requete += "   sodium_mg text,"
requete += "   cuivre_mg text,"
requete += "   proteines_g text,"
requete += "   beta_carotene_µg text,"
requete += "   magnesium_mg text,"
requete += "   phosphore_mg text,"
requete += "   calories_kcal text,"
requete += "   vitamine_c_mg text,"
requete += "   potassium_mg text,"
requete += "   vitamine_b2_mg text,"
requete += "   vitamine_b1_mg text,"
requete += "   manganese_mg text,"
requete += "   zinc_mg text,"
requete += "   vitamine_e_mg text,"
requete += "   vitamine_a_mg text,"
requete += "   fibres_g text,"
requete += "   vitamine_b9_µg text,"
requete += "   lipides_g text,"
requete += "   vitamine_b3_mg text,"
requete += "   vitamine_b6_mg text,"
requete += "   calcium_mg text,"
requete += "   glucides_g text,"
requete += "   vitamine_b5_mg text,"
requete += "   energie_kj text,"
requete += "   fer_mg text,"
requete += "   fibres_alimentaires_g text,"
requete += "   index serial NOT NULL,"
requete += "   PRIMARY KEY (index)"
requete += ")"
cur.execute(requete)
con.commit()

x = 0
while x < 1000:
    # Boucle qui va itérer avec chaque aliment pour remplire la table avec des
    # requetes SQL
    for dictionnaire in dictionnaire_table:
        requete = "INSERT INTO table_avec_requetes (nom, eau_g, vitamine_k_µg, "
        requete += "sodium_mg, cuivre_mg, proteines_g, beta_carotene_µg, "
        requete += "magnesium_mg, phosphore_mg, calories_kcal, vitamine_C_mg, "
        requete += "potassium_mg, vitamine_b2_mg, vitamine_b1_mg, "
        requete += "manganese_mg, zinc_mg, vitamine_e_mg, vitamine_a_mg, "
        requete += "fibres_g, vitamine_b9_µg, lipides_g, vitamine_b3_mg, "
        requete += "vitamine_b6_mg, calcium_mg, glucides_g, vitamine_b5_mg, "
        requete += "energie_kj, fer_mg, fibres_alimentaires_g) VALUES ("
        for colonne, valeur in dictionnaire.items():
            requete += "'" + str(valeur) + "'" + ","
        # Supprime la dernière virgule
        requete = requete[:-1]
        requete += ")"
        cur.execute(requete)
        con.commit()
    x += 1

fin = time.time()
temps = fin - debut

print(temps)  # 17.025208950042725

# Fusion des dataframe pour former 20 fois la table initiale
table_optimisation_decuple = table_optimisation

x = 0
while x < 1000:
    table_optimisation_decuple = (table_optimisation_test.append
                                  (table_optimisation, ignore_index=True))
    x += 1

debut = time.time()
engine = create_engine(
    'postgresql://postgres:digifab@localhost:5432/ProjetPerso')
table_optimisation_decuple.to_sql('table_avec_sqlalchemy', engine, index=False)
fin = time.time()
temps = fin - debut
print(temps)  # 10.72651481628418

fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])
methode = ['Sqlalchemy', 'Requetes SQL']
temps = [10.72651481628418, 17.025208950042725]
ax.bar(methode, temps)
ax.legend(['Temps en secondes'])
plt.title("Temps d'exécution avec Sqlalchemy et Requetes SQL", fontsize=15)
plt.show()

# =============================================================================
#           TEST PERFORMANCE SUR DEUX BDD sur table de 74000 lignes
# =============================================================================

con = psycopg2.connect(host="localhost",
                       database="BDD CERTIF",
                       user="postgres",
                       password="digifab",
                       port=5432)

cur = con.cursor()

debut = time.time()
x = 0
while x != 10000:
    requete = "SELECT aliment.nom_aliment, apport.nom_apport, " \
              "contenir.apport_quantite"
    requete += " FROM contenir"
    requete += " JOIN aliment ON aliment.id_aliment = contenir.id_aliment"
    requete += " JOIN apport ON apport.id_apport = contenir.id_apport"
    requete += " WHERE aliment.nom_aliment = 'yuzu'"
    cur.execute(requete)
    test = cur.fetchall()
    x += 1
fin = time.time()
temps = fin - debut
print(temps)  # 4.939329385757446

con = psycopg2.connect(host="localhost",
                       database="ProjetPerso",
                       user="postgres",
                       password="digifab",
                       port=5432)
cur = con.cursor()

debut = time.time()
x = 0
while x != 10000:
    requete = "SELECT * FROM table_avec_requetes WHERE nom = 'yuzu'"
    con.commit()
    cur.execute(requete)
    test1 = cur.fetchall()
    x += 1
fin = time.time()
temps = fin - debut
print(temps)  # 89.90897727012634

fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])
methode = ['Nouvelle BDD', 'Ancienne BDD']
temps = [4.939329385757446, 89.90897727012634]
ax.bar(methode, temps)
ax.legend(['Temps en secondes'])
plt.title("Temps d'exécution avec la nouvelle BDD et l'ancienne", fontsize=15)
plt.show()
