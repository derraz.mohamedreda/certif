# -*- coding: utf-8 -*-
"""
Created on Fri Aug 14 12:09:06 2020

Constructeur des differents Json pour les graphiques

Chaque graphique à une structure de Json différentes et peut avoir des clés
différentes, le constructeur prends en compte ces différences pour obtenir
un Json fonctionnel pour chaque graphiques.

@author: Reda
"""

import json
import math

import pandas as pd
import psycopg2

con = psycopg2.connect(host="localhost",
                       database="ProjetPerso",
                       user="postgres",
                       password="digifab",
                       port=5432)

sql = "select * from table_aliments3;"
tablealiments = pd.read_sql_query(sql, con)

liste_aliments = tablealiments.index.tolist()

# Il faut changer toutes les valeurs NaN en 0 pour pouvoir afficher les
# graphiques.
for valeurs in tablealiments.columns:
    for aliment in liste_aliments:
        if math.isnan(tablealiments.loc[aliment][valeurs]) is True:
            tablealiments.loc[aliment][valeurs] = 0.0001

# =============================================================================
#                   ECRITURE DU JSON POUR LE DIAGRAMME CIRCULAIRE
# =============================================================================

# Lorsque que les valeurs sont "NaN" le graphique ne s'afficher pas, il faut
# donc ajouter dans le nom de l'aliment "(Null)" et mettre le NaN en string.
# math.isan vérifie si la valeur est un float nan

liste_json = []
liste_data = []
x = 1
for valeurs in tablealiments.columns:
    liste_data = []
    for aliment in liste_aliments:
        if tablealiments.loc[aliment][valeurs] != 0.0001:
            data = {"aliment": aliment,
                    "valeur_nutritionnelle":
                        tablealiments.loc[aliment][valeurs]}
        else:
            continue

        liste_data.append(data)

    structure_json = {
        "label": valeurs,
        "table": "dcirculaires" + str(x),
        "data": liste_data
    }
    liste_json.append(structure_json)
    x += 1

with open('dcirculaire.json', 'w') as outfile:
    json.dump(liste_json, outfile)

# =============================================================================
#                   ECRITURE DU JSON POUR L'HISTOGRAMME
# =============================================================================

liste_json = []
liste_data = []
x = 1

for valeurs in tablealiments.columns:
    liste_data = []
    for aliment in liste_aliments:
        if tablealiments.loc[aliment][valeurs] != 0.0001:
            data = {"aliment": aliment,
                    "valeur_chiffre": tablealiments.loc[aliment][valeurs]}
        else:
            continue

        liste_data.append(data)

    structure_json = {
        "label": valeurs,
        "table": "histogramme" + str(x),
        "data": liste_data
    }
    liste_json.append(structure_json)
    x += 1

with open('histogramme.json', 'w') as outfile:
    json.dump(liste_json, outfile)

# =============================================================================
#                   ECRITURE DU JSON POUR LE SLICED
# =============================================================================

liste_valeurs_sliced = ['Eau g', 'Vitamine K µg', 'Sodium mg', 'Cuivre mg',
                        'Protéines g', 'Bêta-carotène µg', 'Magnésium mg',
                        'Phosphore mg', 'Vitamine C mg', 'Potassium mg',
                        'Vitamine B2 mg', 'Vitamine B1 mg', 'Manganèse mg',
                        'Zinc mg', 'Vitamine E mg', 'Vitamine A mg',
                        'Fibres g', 'Vitamine B9 µg', 'Lipides g',
                        'Vitamine B3 mg', 'Vitamine B6 mg', 'Calcium mg',
                        'Glucides g', 'Vitamine B5 mg', 'Fer mg',
                        'Fibres alimentaires g']

# Pour proportionner correctement les graphiques sliced, il faut convertir
# toutes les unitées de mesures en gramme, diviser par 1000 les mg etc.
table_aliment_sliced = tablealiments

for aliment in liste_aliments:
    for valeur in liste_valeurs_sliced:
        if table_aliment_sliced.loc[aliment][valeur] != 0.0001:

            if "mg" in str(valeur):
                print(table_aliment_sliced.loc[aliment][valeur])
                table_aliment_sliced.loc[aliment][valeur] = (
                        table_aliment_sliced.loc[aliment][valeur] / 1000)

            elif "µg" in str(valeur):
                table_aliment_sliced.loc[aliment][valeur] = (
                        table_aliment_sliced.loc[aliment][valeur] / 1000000)

liste_json = []
liste_data = []
x = 1

for aliment in liste_aliments:
    liste_data = []
    for valeurs in liste_valeurs_sliced:
        if " g" in str(valeurs):
            if table_aliment_sliced.loc[aliment][valeurs] != 0.0001:
                data = {"valeur": valeurs,
                        "valeur_chiffre":
                            table_aliment_sliced.loc[aliment][valeurs]}
            else:
                continue

            liste_data.append(data)

    # Trie les valeurs en ordre décroissant pour avoir un graphique plus
    # lisible
    data_ordre_decroissant = sorted(liste_data, key=lambda i:
    i['valeur_chiffre'], reverse=True)

    structure_json = {
        "label": aliment,
        "table": "sliced" + str(x),
        "data": data_ordre_decroissant
    }
    liste_json.append(structure_json)
    x += 1

with open('sliced.json', 'w') as outfile:
    json.dump(liste_json, outfile)

# =============================================================================
#                   ECRITURE DU JSON POUR LE SLICED_MICRO
# =============================================================================

liste_json = []
liste_data = []

x = 1
for aliment in liste_aliments:
    liste_data = []
    for valeurs in liste_valeurs_sliced:
        if " g" not in str(valeurs):
            if table_aliment_sliced.loc[aliment][valeurs] != 0.0001:
                data = {"valeur": valeurs,
                        "valeur_chiffre":
                            table_aliment_sliced.loc[aliment][valeurs]}
            else:
                continue

            liste_data.append(data)

    data_ordre_decroissant = sorted(liste_data, key=lambda i:
    i['valeur_chiffre'], reverse=True)

    structure_json = {
        "label": aliment,
        "table": "sliced" + str(x),
        "data": data_ordre_decroissant
    }
    liste_json.append(structure_json)
    x += 1

with open('sliced_micro.json', 'w') as outfile:
    json.dump(liste_json, outfile)

# =============================================================================
#                   ECRITURE DU JSON POUR LES TABLEAUX (PAR ALIMENTS)
# =============================================================================

liste_json = []
liste_data = []

x = 1
for aliment in liste_aliments:
    liste_data = []
    data1 = []
    for valeurs in tablealiments.columns:
        data = [tablealiments.loc[aliment][valeurs]]
        data1 += data
    data1 = [aliment] + data1
    liste_data.append(data1)

    structure_json = {
        "table": "tableJS" + str(x),
        "data": liste_data
    }
    liste_json.append(structure_json)
    x += 1

with open('table.json', 'w') as outfile:
    json.dump(liste_json, outfile)

# =============================================================================
#                   ECRITURE DU JSON POUR LE TABLEAU (ENTIER)
# =============================================================================

liste_json = []
liste_data = []

for aliment in liste_aliments:
    data1 = []
    for valeurs in tablealiments.columns:
        data = [tablealiments.loc[aliment][valeurs]]
        data1 += data
    data1 = [aliment] + data1
    liste_data.append(data1)

structure_json = {
    "table": "tablediv",
    "data": liste_data
}

liste_json.append(structure_json)

with open('table.json', 'w') as outfile:
    json.dump(liste_json, outfile)

# =============================================================================
#                   ECRITURE DU JSON POUR LE TABLEAUX RECETTE
# =============================================================================

dict_data = {'Protéines g': 6.1499999999999995,
             'Calories kcal': 205.0,
             'Lipides g': 1.6400000000000001,
             'Glucides g': 47.97,
             'Fibres alimentaires g': 30}

liste_data = []
liste_json = []

for valeur_nutri, valeur in dict_data.items():
    data = [valeur_nutri]
    data += [valeur]
    liste_data.append(data)

structure_json = {
    "table": "table_div",
    "data": liste_data
}

liste_json.append(structure_json)

with open('table_recette.json', 'w') as outfile:
    json.dump(liste_json, outfile)
