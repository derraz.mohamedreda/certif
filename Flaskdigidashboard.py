# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 21:25:48 2020

Module Flask permettant à un utilisateur de créer un dashboard simplement
à l'aide du wrapper (DigiDashBoard.py).

L'utilisateur n'aura pas besoin d'utiliser Flask pour créer son dashboard,
seulement d'appeler les classes du wrapper (DigiDashBoard.py).

@author: Reda
"""
import datetime
import os

import pandas as pd
import psycopg2
from flask import (Flask, render_template, request, json, session, redirect,
                   url_for)

app = Flask(__name__)
app.config['SECRET_KEY'] = "çàt.z#t'eqàç#tgeç(àgeç#gà(t^dze'rw"
app.jinja_env.add_extension('jinja2.ext.do')

boutonstitres = {}
graphiques = {}
fonctionnalites = {}


@app.route('/')
@app.route('/accueil')
def accueil():
    return render_template('accueil.html', title='Accueil',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)


# Route du graphique Histogramme
@app.route('/Histogramme')
def histogrammes():
    return render_template('Histogramme.html', title='Histogrammes',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)


# Cette fonction permet la récupération du JSON présent dans la racine du
# dossier, puis avec l'app.route de le retouner pour JS grace au "GET".
# "/datahistogrammes" est la route utilisée dans le fichier JS.
@app.route('/datahistogrammes', methods=['GET'])
def datahistogrammes():
    hist1 = open('json/histogrammes/histogramme.json', )
    histogramme1 = json.load(hist1)
    histodump = json.dumps(histogramme1)

    return histodump


# Route du graphique Sliced
@app.route('/Sliced')
def sliced():
    return render_template('Sliced.html', title='Sliced',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)


# Recuperation du Json pour le graphique Sliced
@app.route('/datasliced', methods=['GET'])
def datasliced():
    sliced = open('json/sliced/sliced.json', )
    sliced1 = json.load(sliced)
    sliceddump = json.dumps(sliced1)

    return sliceddump


# Route du graphique Sliced micronutriment
@app.route('/Sliced_micro')
def sliced_micro():
    return render_template('Sliced_micro.html', title='Sliced_micro',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)


# Recuperation du Json pour le graphique Sliced micronutriment
@app.route('/datasliced_micro', methods=['GET'])
def datasliced_micro():
    sliced_micro = open('json/sliced_micro/sliced_micro.json', )
    sliced_micro1 = json.load(sliced_micro)
    sliced_microdump = json.dumps(sliced_micro1)

    return sliced_microdump


# Route du graphique Courbe
@app.route('/Courbe')
def courbe():
    return render_template('Courbe.html', title='Courbe',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)


# Recuperation du Json pour le graphique Courbe
@app.route('/datacourbe', methods=['GET'])
def datascourbe():
    cour1 = open('json/courbes/courbe_defaut.json', )
    courbe = json.load(cour1)
    courbedump = json.dumps(courbe)

    return courbedump


# Route du graphique Diagramme circulaire
@app.route('/Diagrammes')
def diagrammes_circulaires():
    return render_template('Diagrammes_circulaires.html',
                           title='Diagrammes circulaires',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)


# Recuperation du Json pour le graphique Diagramme circulaire
@app.route('/datadiagrammes_circulaires', methods=['GET'])
def datadiagrammes_circulaires():
    dcirc = open('json/dcirculaires/dcirculaire.json', )
    dcirculaire = json.load(dcirc)
    dcirculairedump = json.dumps(dcirculaire)

    return dcirculairedump


# Route de la table
@app.route('/Tableau')
def table():
    return render_template('Table.html', title='Table',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)


# Recuperation du Json pour la table
@app.route('/datatable', methods=['GET'])
def datatable():
    table = open('json/tables/table.json', )
    tablej = json.load(table)
    tablejsonn = json.dumps(tablej)

    return tablejsonn


# Test d'une table interactive (abandonné)
@app.route('/tableinteractivetest')
def tableinteractivetest():
    return render_template('Tableinteractive.html',
                           title='Tableinteractive',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)


# Fonction qui renvoie les 5 aliments ayant la valeur nutritionnelle
# resenseignée la plus haute.
@app.route('/Recommandations', methods=['GET', 'POST'])
def recommandation_get():
    con = psycopg2.connect(host="localhost",
                           database="BDD CERTIF",
                           user="postgres",
                           password="digifab",
                           port=5432)

    sql1 = "SELECT * FROM apport"
    sql3 = "SELECT aliment.nom_aliment, apport.nom_apport, " \
           "contenir.apport_quantite"
    sql3 += " FROM contenir"
    sql3 += " JOIN aliment ON aliment.id_aliment = contenir.id_aliment"
    sql3 += " JOIN apport ON apport.id_apport = contenir.id_apport"

    table_apport = pd.read_sql_query(sql1, con)
    table_aliment_complete = pd.read_sql_query(sql3, con)

    liste_valeurs = table_apport['nom_apport'].to_list()

    if request.method == 'GET':

        # Retourne la liste en fonction de la valeur nutritionelle renseignée.
        return render_template('recommandation.html',
                               title='recommandation',
                               boutonstitres=boutonstitres,
                               graphiques=graphiques,
                               fonctionnalites=fonctionnalites,
                               liste_valeurs=liste_valeurs)

    elif request.method == 'POST':

        valeur_nutritionnelle = request.form['valeur nutritionnelle']
        print(valeur_nutritionnelle)

        # valeur_nutritionnelle = "fer_mg"

        # Permet d'avoir la colonne d'apport d'abricot
        # (table_aliment_complete.loc[table_aliment_complete['nom_aliment'] == 
        #                         'abricot']['apport_quantite'])

        # Contient les 5 aliments ayant la valeur nutritionnelle resenseignée 
        # la plus haute.
        liste_aliments_max = []

        # Change l'ordre des lignes de manière decroissante, les lignes ayant 
        # les valeurs les plus hautes se trouve en tête de la dataframe.
        aliments_max_decroissant = (table_aliment_complete.sort_values
                                    (['apport_quantite'], ascending=False)
                                    .groupby('nom_apport').head(5))

        # Place les 5 premieres lignes de la dataframe ayant pour valeur la
        # valeur nutritionnelle renseignée dans la variable 'aliments_max_5'.
        aliments_max_5 = (aliments_max_decroissant.loc[aliments_max_decroissant
                                                       [
                                                           'nom_apport'] ==
                                                       valeur_nutritionnelle])

        # Itère sur les index de la variable aliments_max_5 (les aliments), 
        # puis ajoute le nom des index dans la liste 'liste_aliments_max', le 
        # nom des index sont en fait les aliments.
        for aliment in aliments_max_5['nom_aliment']:
            liste_aliments_max.append(aliment)
        print(liste_aliments_max)

        return render_template('recommandation.html',
                               title='recommandation',
                               boutonstitres=boutonstitres,
                               graphiques=graphiques,
                               fonctionnalites=fonctionnalites,
                               liste_aliments_max=liste_aliments_max,
                               liste_valeurs=liste_valeurs,
                               valeur_nutritionnelle=valeur_nutritionnelle)


# =============================================================================
#                               FONCTION RECETTE
# =============================================================================


# Si le dictionnaire recette existe (print pour verifier) alors fait juste un
# print, sinon contre le dictionnaire recette.
# Ce try permet d'acceder à la page recette même si aucune recette est 
# renseignée.
""" L'ERREUR EST "NORMAL". """
try:
    print(recette)
except:
    recette = {}
    print("j'ai reset la recette")

try:
    print(dictionnaire_total_valeur)
except:
    dictionnaire_total_valeur = {}
    print("j'ai reset le dictionnaire_total_valeur")


# Fonction qui retourne la quantité des valeurs nutritionnelles de la quantité
# de chaque aliment renseigné.
# Puis les additionnes pour obtenir un total de tous les apports nutritionnels
# de la recette.
@app.route('/Recettes', methods=['GET', 'POST'])
def recette_get():
    con = psycopg2.connect(host="localhost",
                           database="BDD CERTIF",
                           user="postgres",
                           password="digifab",
                           port=5432)

    sql1 = "SELECT * FROM apport"
    sql2 = "SELECT * FROM aliment"
    sql3 = "SELECT aliment.nom_aliment, apport.nom_apport, " \
           "contenir.apport_quantite"
    sql3 += " FROM contenir"
    sql3 += " JOIN aliment ON aliment.id_aliment = contenir.id_aliment"
    sql3 += " JOIN apport ON apport.id_apport = contenir.id_apport"

    table_apport = pd.read_sql_query(sql1, con)
    table_aliment = pd.read_sql_query(sql2, con)
    table_aliment_complete = pd.read_sql_query(sql3, con)

    liste_valeurs = table_apport['nom_apport'].to_list()
    liste_aliments = table_aliment['nom_aliment'].to_list()

    # Les valeurs des aliments sont au 100 grammes, le calcul suivant permet
    # d'avoir sa valeur au gramme.
    # La variable 'tablealiments_recette' va contenir la table_aliment, elle 
    # aura ses valeurs divisées par 100.
    tablealiments_recette = table_aliment_complete

    # Remplace la colonne 'apport_quantite' par la colonne 'apport_quantite'
    # divisée par 100 avec 3 chiffres après la virgule
    tablealiments_recette['apport_quantite'] = (
        tablealiments_recette['apport_quantite'].div(100).round(3))

    if request.method == 'GET':

        return render_template('recette.html',
                               title='recette',
                               boutonstitres=boutonstitres,
                               graphiques=graphiques,
                               fonctionnalites=fonctionnalites,
                               liste_valeurs=liste_valeurs,
                               liste_aliments=liste_aliments,
                               recette=recette,
                               dictionnaire_total_valeur=
                               dictionnaire_total_valeur)

    elif request.method == 'POST':

        # Attribution des valeurs récuperer avec la méthode poste.
        aliment = request.form['aliment']
        print(aliment)
        quantitees = request.form['quantitees']
        # Les informations de quantitées ont besoin d'être des integer et non
        # des string pour être utilisé dans le tableau.
        quantitees = int(quantitees)
        print(quantitees)

        # Remplissage du dictionnaire de recette avec l'aliment pour clé
        # et la quantitée pour valeur.
        recette[aliment] = quantitees

        # Ressort une liste de listes de l'aliment avec pour chaque list:
        # le nom de l'aliment, la valeur nutritionnelle, la quantité
        # exemple : [['yuzu', 'eau_g', 0.889],['yuzu', 'zin_mg', 0.0]...]
        # (table_aliment_complete.loc[table_aliment_complete['nom_aliment'] ==
        #                         'yuzu'].values)

        # 'tous_les_aliments' est un dictionnaire contenant pour clé l'aliment, 
        # et comme valeur le dictionnaire toutes_les_valeurs_et_quantitees.
        # 'toutes_les_valeurs_et_quantitees' est un dictionnaire regroupant
        # toutes les valeurs et quantitees (non null) de l'aliment renseigné
        # par l'utilisateur.
        tous_les_aliments = {}
        toutes_les_valeurs_et_quantitees = {}

        array_aliment = (table_aliment_complete.loc[table_aliment_complete
                                                    [
                                                        'nom_aliment'] ==
                                                    aliment].values)
        for liste in array_aliment:
            # Si l'aliment n'est pas égale à 0.000 (qui est en fait un 
            # null)
            if liste[2] != 0.0:
                # Multiplie la valeur de la vauleur_nutri de l'aliment par 
                # sa quantite
                quantite_valeur = (liste[2] * quantitees)
                # Ajoute dans le dictionnaire
                # 'toutes_les_valeurs_et_quantitees' la quantité de la 
                # valeur nutritionnelle de l'aliment renseigné par 
                # l'utilisateur
                toutes_les_valeurs_et_quantitees[liste[1]] = (
                    quantite_valeur)

        # Place le dictionnaire 'toutes_les_valeurs_et_quantitees' dans
        # la valeur aliment du dictionnaire tous_les_aliments
        tous_les_aliments[aliment] = toutes_les_valeurs_et_quantitees

        # Reinitisalise le dictionnaire pour reset les valeurs déjà 
        # présentes
        toutes_les_valeurs_et_quantitees = {}

        # Additionne toutes les valeurs d'une même valeur nutrionnelle des 
        # aliments renseignés. Pour avoir un total des apports nutritionnelles 
        # dans une recette.

        for aliment in tous_les_aliments:
            # print(aliment)
            for key, value in tous_les_aliments[aliment].items():
                print(key, value)
                if key in dictionnaire_total_valeur.keys():
                    dictionnaire_total_valeur[key] = (dictionnaire_total_valeur
                                                      [key] + value)
                else:
                    dictionnaire_total_valeur[key] = value
        # print(dictionnaire_total_valeur) 

        # Construction du Json à partir des données du 
        # dictionnaire_total_valeur.
        liste_data = []
        liste_json = []

        for valeur_nutri, valeur in dictionnaire_total_valeur.items():
            data = [valeur_nutri]
            data += [valeur]
            liste_data.append(data)

        structure_json = {
            "table": "table_div",
            "data": liste_data
        }

        liste_json.append(structure_json)

        with open('json/tables/table_recette.json', 'w') as outfile:
            json.dump(liste_json, outfile)

        # Permet d'afficher là page mais si 'delete' n'est pas renseigné.
        try:
            delete = request.form['delete']
        except:
            print("pas de delete")

        # Si 'delete' est renseigné, supprime le fichier Json actuel,
        # reinitialise les valeurs du dictionnaire de recette et ajoute
        # un json vide dans le fichier pour ne pas avoir un bloc blanc
        # à la place d'un tableau.
        if delete == 'delete':
            os.remove('json/tables/table_recette.json')
            print("delete")
            recette.clear()
            dictionnaire_total_valeur.clear()

            structure_json = {
                "table": "table_div",
                "data": []
            }

            liste_json.append(structure_json)

            with open('json/tables/table_recette.json', 'w') as outfile:
                json.dump(liste_json, outfile)

    # print(recette)
    return render_template('recette.html',
                           title='recette',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites,
                           recette=recette,
                           liste_valeurs=liste_valeurs,
                           liste_aliments=liste_aliments,
                           dictionnaire_total_valeur=dictionnaire_total_valeur)


# Recuperation du Json pour la table
@app.route('/datatablerecette', methods=['GET'])
def datatable_recette():
    table_recette = open('json/tables/table_recette.json', )
    table_recettej = json.load(table_recette)
    table_recettejsonn = json.dumps(table_recettej)

    return table_recettejsonn


# Permet l'ajout des recettes dans la table recette
@app.route('/recette_table', methods=['GET', 'POST'])
def recette_table():
    con = psycopg2.connect(host="localhost",
                           database="BDD CERTIF",
                           user="postgres",
                           password="digifab",
                           port=5432)
    cur = con.cursor()
    sql1 = "SELECT * FROM apport"
    sql2 = "SELECT * FROM aliment"
    sql3 = "SELECT aliment.nom_aliment, apport.nom_apport, " \
           "contenir.apport_quantite"
    sql3 += " FROM contenir"
    sql3 += " JOIN aliment ON aliment.id_aliment = contenir.id_aliment"
    sql3 += " JOIN apport ON apport.id_apport = contenir.id_apport"

    table_apport = pd.read_sql_query(sql1, con)
    table_aliment = pd.read_sql_query(sql2, con)

    liste_valeurs = table_apport['nom_apport'].to_list()
    liste_aliments = table_aliment['nom_aliment'].to_list()

    # Place dans un dictionnaire les aliments avec leurs id
    dict_aliment_simple = {}
    dict_table_aliment = table_aliment.to_dict("records")

    for dictionnaire in dict_table_aliment:
        id_aliment = dictionnaire['id_aliment']
        nom_aliment = dictionnaire['nom_aliment']
        dict_aliment_simple[nom_aliment] = id_aliment

    # Remplissage de la table recette 
    if request.method == 'POST':
        requetemax = "SELECT MAX(id_recette) FROM recette"
        cur.execute(requetemax)
        idmax = cur.fetchall()
        idmax = idmax[0][0]
        idmaxtrue = isinstance(idmax, int)

        if idmaxtrue is True:
            idmax = 1 + idmax
        else:
            idmax = 1

        mail_utilisateur = session['user']['email']

        requete = "SELECT id_utilisateur FROM utilisateur WHERE mail = " + \
                  "'" + str(
            mail_utilisateur) + "'"
        cur.execute(requete)
        id_utilisateur = cur.fetchall()
        id_utilisateur = id_utilisateur[0][0]
        for aliment, quantite in recette.items():
            id_aliment = dict_aliment_simple[aliment]
            requete = "INSERT INTO recette (id_utilisateur, id_recette,"
            requete += " recette_quantite, id_aliment) VALUES ("
            requete += str(id_utilisateur) + ", " + str(idmax) + ", "
            requete += str(quantite) + ", " + str(id_aliment)
            requete += ");"
            cur.execute(requete)
            con.commit()

        if recette != {}:
            file1 = open(
                "Sauvegarde bdd et metadonnees\metadonnees\log_recette.txt",
                "a")
            metarecette = "La recette " + str(
                recette) + " à été enregistrée le : " + str(
                datetime.datetime.now()) + " par l'utilisateur " + str(
                id_utilisateur) + "\n"
            file1.write(metarecette)
            file1.close()
    print(recette)

    return render_template('recette.html',
                           title='recette',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites,
                           recette=recette,
                           liste_valeurs=liste_valeurs,
                           liste_aliments=liste_aliments)


# =============================================================================
#                               LOGIN 
# =============================================================================

# Ajout d'un system d'inscription
@app.route('/inscription', methods=['GET', 'POST'])
def inscription():
    con = psycopg2.connect(host="localhost",
                           database="BDD CERTIF",
                           user="postgres",
                           password="digifab",
                           port=5432)
    cur = con.cursor()

    # Récuperation des données du formulaire d'inscription
    if request.method == 'POST':
        nom = request.form['Nom']
        prenom = request.form['Prenom']
        mot_de_passe = request.form['mdp']
        email = request.form['email']

        print(nom, prenom, mot_de_passe, email)

        requete = "SELECT*FROM utilisateur WHERE mail = " + "'" + str(
            email) + "'"
        cur.execute(requete)
        utilisateur = cur.fetchall()
        if utilisateur == []:
            requete = "INSERT INTO utilisateur (id_role, nom_utilisateur, " \
                      "prenom_utilisateur, mail, mot_de_passe) "
            requete += "VALUES (1, " + "'" + str(nom) + "'" + ", " + "'" + str(
                prenom) + "'" + ", " + "'" + str(
                email) + "'" + ", " + "'" + str(mot_de_passe) + "'"
            requete += ")"
            cur.execute(requete)
            con.commit()

            # Ecriture de la metadonne
            file3 = open(
                "Sauvegarde bdd et metadonnees\metadonnees\log_inscription.txt",
                "a")
            metainscription = "L'utilisateur " + str(
                nom) + ", " + str(prenom) + " avec pour adresse e-mail " + str(
                email) + " c'est inscrit le : " + str(
                datetime.datetime.now()) + "\n"
            file3.write(metainscription)
            file3.close()
            return render_template('inscription.html',
                                   title='Inscription',
                                   nom=nom,
                                   prenom=prenom,
                                   mot_de_passe=mot_de_passe,
                                   email=email,
                                   boutonstitres=boutonstitres,
                                   graphiques=graphiques,
                                   fonctionnalites=fonctionnalites)
        else:
            return redirect(url_for('emailfalse'))

    else:
        return render_template('inscription.html', title='Inscription',
                               boutonstitres=boutonstitres,
                               graphiques=graphiques,
                               fonctionnalites=fonctionnalites)


# Ajout d'un stystem de login
@app.route('/login', methods=['GET', 'POST'])
def login():
    con = psycopg2.connect(host="localhost",
                           database="BDD CERTIF",
                           user="postgres",
                           password="digifab",
                           port=5432)
    cur = con.cursor()
    if request.method == 'POST':
        mot_de_passe = request.form['mdp']
        email = request.form['email']

        try:
            requete = "SELECT*FROM utilisateur WHERE mail = " + "'" + str(
                email) + "'" + "and mot_de_passe = " + "'" + str(
                mot_de_passe) + "'"
            cur.execute(requete)
            utilisateur = cur.fetchall()
            prenom = utilisateur[0][3]
            email = utilisateur[0][4]

            requete = "SELECT id_role FROM utilisateur WHERE mail = " + "'" + \
                      str(
                          email) + "'" + "and mot_de_passe = " + "'" + str(
                mot_de_passe) + "'"
            cur.execute(requete)
            role = cur.fetchall()
            session['user'] = {
                'username': str(prenom),
                'email': str(email),
                'role': role[0][0]
            }
            session['user']

            # Ecriture de la metadonne
            file2 = open(
                "Sauvegarde bdd et metadonnees\metadonnees\log_login.txt",
                "a")
            metalogin = "L'utilisateur " + str(
                utilisateur) + " c'est connecté le : " + str(
                datetime.datetime.now()) + "\n"
            file2.write(metalogin)
            file2.close()
        except:
            return "Votre adresse mail ou mot de passe est incorrect"

    return render_template('login.html', title='Login',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)


# Deconnexion
@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('accueil'))


# Erreur si l'e-mail renseigne est deja utilise
@app.route('/emailfalse')
def emailfalse():
    return "Cette adresse e-mail est déjà utilisée"


# Fonction d'update de la table utilisateur, uniquement accessible par l'admin
@app.route('/administrateur', methods=['GET', 'POST'])
def administrateur():
    con = psycopg2.connect(host="localhost",
                           database="BDD CERTIF",
                           user="postgres",
                           password="digifab",
                           port=5432)
    cur = con.cursor()

    if request.method == 'POST':
        id_utilisateur = request.form['id_utilisateur']
        email = request.form['email']
        nom = request.form['Nom']
        prenom = request.form['Prenom']
        mot_de_passe = request.form['mdp']

        requete = "UPDATE utilisateur SET nom_utilisateur = " + "'" + str(
            nom) + "'" + " , prenom_utilisateur = " + "'" + str(
            prenom) + "'" + " , mail = " + "'" + str(
            email) + "'" + " , mot_de_passe = " + "'" + str(mot_de_passe) + "'"
        requete += " WHERE id_utilisateur = " + str(id_utilisateur)
        cur.execute(requete)
        con.commit()

    return render_template('administrateur.html', title='Administration',
                           boutonstitres=boutonstitres,
                           graphiques=graphiques,
                           fonctionnalites=fonctionnalites)
