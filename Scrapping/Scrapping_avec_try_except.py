# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 15:54:38 2020

Scraping des tableaux sur les différents fruits et légumes du site
Passeportsante.

Une fois traitées, ces données seront placées dans une base de données Postgre
pour y être visualiser et utiliser sur Digidashboard.

Plan :

I - Faire le tutoriel de selenium

II - Scraper un premier tableau
    a - Le transformer en datafram
    b - Le transformer en excel

III - Boucler la méthode de scrap utilisé pour le premier tableau

IV - Créer une Dataframe
    a - Une première colonne avec le nom des fruits/légumes
    b - Trier les noms des autres colonnes (vitamines, minéraux etc)
    c - Ajouter les colonnes dans la dataframe
    d - Trier les valeurs des tableaux scrapés
    e - Remplir la Dataframe par les valeurs triées

V - Sortir la Dataframe complère
    a - En csv ou excel
    b - Dans une table sur postgre

VI - Visualiser les données de la table sur DIGIDASHBOARD

VII - Intéraction avec les données
    a - Fonctionnalité de carences
    b - Peut être d'autres fonctionnalitées


TODO : Ne pas oublier de scraper les fruits manquants sur :
   https://www.passeportsante.net/fr/Nutrition/EncyclopedieAliments/Index.aspx

@author: Reda
"""

import time
# from pandas import ExcelWriter
from datetime import datetime

import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from sqlalchemy import create_engine

# Utilisation de webdriver.exe pour permmetre à selenium de lancer Chrome
PATH = r"D:\Boscope\Documents\ProjetPersoReda\Scrapping\chromedriver.exe"
driver = webdriver.Chrome(PATH)

# Page de base pour commencer les actions de selenium
driver.get("https://www.passeportsante.net/les-fruits-et-legumes-g153")

# Navigation sur les pages web du site, avec les cliques et les backpage.
# La navigation se fait soir par l'xpath, soit par l'id, le time sleep permet
# de laisser le temps à la page de se charger correctement.
# Puis on transforme le tableau en dataframe et en excel.

# Liste des tableaux scrapés
tableaux_scrapes = []

# Liste des fruits et légumes
fruits_legumes = []

# Liste des valeurs nutritives
valeurs_nutritives = []

listealiments = [
    "Abricot", "Acérola", "Airelles", "Ananas", "Argousier",
    "Artichaut", "Asperge", "Aubergine", "Avocat", "Baie de goji",
    "Banane", "Bette à carde", "Betterave", "Brocoli", "Brugnon",
    "Canneberge", "Carambole", "Carotte", "Cassis", "Chou chinois",
    "Chou frisé", "Chou-rave", "Chou", "Choux de Bruxelles",
    "Citrouille", "Clémentine", "Coing", "Combava",
    "Concombre et Cornichon", "Courges", "Courgette", "Céleri",
    "Datte", "Endive", "Epinards", "Fenouil", "Figue", "Fraise",
    "Framboise", "Fruit de la passion", "Goyave", "Grenade",
    "Griottes", "Groseille", "Haricot vert", "Kaki", "Kiwi",
    "Kumquat", "Laitue", "Litchi", "Mangue", "Melon", "Mirabelle",
    "Myrtille", "Mâche", "Noix de coco", "Pamplemousse",
    "Pastèque", "Patate douce", "Poire", "Poireau", "Poivron",
    "Pomelo", "Pomme de terre", "Prune et pruneau", "Pêche",
    "Quetsche", "Radis", "Raisin", "Rhubarbe", "Roquette",
    "Tomate", "Topinambour", "Yuzu"
]

date_creation_fichier = datetime.now().strftime("%d-%m-%Y_%I-%M-%S")

# Le scraping se fait par ordre alphabétique (legumes et fruits commençant par
# la lettre "A", la lettre "B" etc).
# Certains éléments d'une lettre peuvent ne pas être un légume ou un fruit
# mais par exemple un conseil de nutrition, c'est pour cette raison qu'il peut
# y avoir plusieurs boucles pour une même lettre.

try:
    # Clique sur "accepter" dans une alerte de cookie
    element = WebDriverWait(
        driver, 10).until(
        (EC.presence_of_element_located(
            (By.XPATH, '//button[@class="didomi-'
                       'components-button didomi-button didomi-dismiss-button '
                       'didomi-'
                       'components-button--color didomi-button-highlight '
                       'highlight-button"]'))))
    element.click()

    # =============================================================================
    # ----- Récupération des aliments commençant par          "A"
    # -----------------
    # =============================================================================

    x = 0
    y = 2
    while x < 15:
        try:
            element = WebDriverWait(
                driver, 10).until(
                (EC.element_to_be_clickable(
                    (By.ID, "cph_ContenuFichePlaceHolder_"
                            "LetterIndexes_LetterIndexRepeater_LetterIndex_2_rptColumns_2_"
                            "rptDocuments_0_lnkDoc_" + str(x) + ""))))
            fruits_legumes.append(element.text)
            element.click()

            time.sleep(1)
            df = pd.read_html(driver.current_url)
            df = df[1]
            # df.to_excel(str(date_creation_fichier)+" "+str(x)+'.xlsx')
            tableaux_scrapes.append(df)

            driver.back()
            # time.sleep(1)

            element = WebDriverWait(
                driver, 10).until(
                (EC.element_to_be_clickable(
                    (By.ID, "cph_ContenuFichePlaceHolder_"
                            "LetterIndexes_LetterIndexRepeater_LetterIndex_2_rptColumns_2_"
                            "rptDocuments_1_lnkDoc_" + str(x) + ""))))
            fruits_legumes.append(element.text)
            element.click()

            # time.sleep(1)
            df = pd.read_html(driver.current_url)
            df = df[1]
            # df.to_excel(str(date_creation_fichier)+" "+str(x)+'.xlsx')
            tableaux_scrapes.append(df)

            driver.back()
            time.sleep(1)

            x += 1
            if x == 15:
                x = 0
            if y == 20:
                var = y == 0
            print(x)

            element = WebDriverWait(
                driver, 10).until(
                (EC.element_to_be_clickable(
                    (By.ID, "cph_ContenuFichePlaceHolder_"
                            "LetterIndexes_LetterIndexRepeater_LetterIndex_2_rptColumns_3_"
                            "rptDocuments_2_lnkDoc_" + str(x) + ""))))
            fruits_legumes.append(element.text)
            element.click()

            # time.sleep(1)
            df = pd.read_html(driver.current_url)
            df = df[1]
            # df.to_excel(str(date_creation_fichier)+" "+str(x)+'.xlsx')
            tableaux_scrapes.append(df)

            driver.back()
            time.sleep(1)

        except BaseException:
            continue
finally:
    print("Terminé")

# =============================================================================
# ----------------------------------- TRIE ------------------------------------
# =============================================================================

# Ajout d'une colonne "Nom" avec pour valeur les noms de la liste
#  "fruits_legumes" dans une Dataframe, à l'aide d'un dictionnaire.
dico_colonne_nom = {"Nom": []}

for fruit_legume in fruits_legumes:
    dico_colonne_nom["Nom"].append(fruit_legume)

tableau_des_aliments = pd.DataFrame(dico_colonne_nom)
print(tableau_des_aliments)

# Itère sur les lignes de la première colonne de chaque dataframe dans la liste
# "tableaux_scrapes".
# Ajoute les noms de colonnes à la liste "valeurs_nutritives".
for i in tableaux_scrapes:
    for ligne in i.index:
        # print(ligne)
        print(i.loc[ligne, 0])
        valeurs_nutritives.append(i.loc[ligne, 0])

# Suppression des noms de colonnes en doublons.
valeurs_nutritives = list(set(valeurs_nutritives))

# Trie les noms d'une liste en fonction de leurs nombre de lettres.
# J'ignore la raison pour le moment, mais pour supprimer les valeurs
# nutritionelles correctement, la boucle for doit être lancer plusieurs fois...
x = 0
while x < 4:
    for valeur_nutritive in valeurs_nutritives:
        if len((str(valeur_nutritive))) > 19:
            valeurs_nutritives.remove(valeur_nutritive)
            x += 1

# Supression des colonnes inutiles restantes.
valeurs_nutritives.remove('Pouvoir\xa0antioxydant')
valeurs_nutritives.remove('Charge glycémique\xa0:')
valeurs_nutritives.remove('Pour 28 g ou 75 ml')

# Supprime la colonne NaN.
del valeurs_nutritives[0]
print(valeurs_nutritives)

# =============================================================================
# ------------------------ CONSTRUCTION DE LA DATAFRAME -----------------------
# =============================================================================


# Ajout des colonnes dans la Dataframe.
for valeur_nutritive in valeurs_nutritives:
    tableau_des_aliments[valeur_nutritive] = ''

# =============================================================================
# ------------------------ REMPLISSAGE DE LA DATAFRAME ----------------------
# =============================================================================

tablealiments.to_excel("tableau_des_aliments.xlsx")

# Reindex les nom et fruit
tablealiments = tableau_des_aliments
tablealiments = tablealiments.reindex(tablealiments.Nom)
tablealiments = tablealiments.drop('Nom', axis=1)

# La liste des dataframes(aliment) dans un dictionnaire :
#   clé = nom de l'aliment et valeur = la dataframe de l'aliment
for dataframe, aliment in zip(tableaux_scrapes, listealiments):
    dicoaliment[aliment] = dataframe

# On remplis la dataframe principale par les valeurs des dataframes des
# aliments
for aliment, dataframe in dicoaliment.items():
    x = 0
    while x < len(dataframe):
        try:
            tablealiments.loc[aliment, dicoaliment[aliment].loc[x, 0]] = (
                dicoaliment[aliment].loc[x, 1])
        except BaseException:
            continue
        x += 1

# On supprime les colonnes inutiles, par le nombres de lettre, et par leurs
# noms directement (pour une raison que j'ignore la totalité n'est pas
# supprimée)
x = 0
while x < 4:
    for valeur_nutritive in tablealiments.columns:
        print(valeur_nutritive)
        if len((str(valeur_nutritive))) > 19:
            tablealiments = tablealiments.drop([valeur_nutritive], axis=1)
            x += 1

tablealiments = tablealiments.drop(['Pouvoir\xa0antioxydant'], axis=1)
tablealiments = tablealiments.drop(['Charge glycémique\xa0:'], axis=1)
tablealiments = tablealiments.drop(['Pour 28 g ou 75 ml'], axis=1)
tablealiments = tablealiments.drop([tablealiments.columns[34]], axis=1)
tablealiments = tablealiments.drop(['Charge glycémique'], axis=1)
tablealiments = tablealiments.drop(['Nutriments'], axis=1)
tablealiments = tablealiments.drop(['Pour 100 g'], axis=1)

# Création de la table.
engine = create_engine(
    'postgresql://postgres:digifab@localhost:5432/ProjetPerso')
tablealiments.to_sql('Aliments', engine)

# Création du fichier excel
tablealiments.to_excel("tableau_des_aliments.xlsx")

# Création d'excel par aliment
for i, x in zip(tableaux_scrapes, fruits_legumes):
    print(x)
    print(i)
    i.to_excel(str(x) + ".xlsx")
