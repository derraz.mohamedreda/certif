# ![enter image description here](https://i.ibb.co/Rjj7Hjb/Logo-digidashboard.png)

  

Digidashboard est un framework.

  

Il donne la possibilité de lancer une application web (un dashboard particulièrement) en peu de temps avec un dictionnaire en Python.

  

L'objectif du framework est de combiner personnalisation et déploiement rapide d'une application web (en une journée maximum).

  

À cela s'ajoute l'utilisation du framework pour un projet de nutrition, le but étant de visualiser des données alimentaires et leurs valeurs nutritionnelles, et de proposer une liste de fruits et légumes contenant le plus d'un élément nutritionnelle renseigné par l'utilisateur.

  

## Conditions

  

Digidashboard fonctionne sur un framework web python, Flask.

Les données sont visualisées en Javascript avec sa bibliothèque AmChart et sont sous la forme d'un Json.

Voici la liste des technologies utilisés :

  

- Panda

- Flask

- Javascript

- Jinja2

- HTML 5

- CSS3

- SASS (compilateur CSS)

  

Et les technologies en plus utilisées avec le projet nutritionnelle:

  

- Selenium

- Sqlalchemy

  

## Fonctionnement

  

Voici un exemple simple montrant la page principale de lancement de l'application

  

![enter image description here](https://i.ibb.co/StSt4L0/screen-page-utilisateur.png)

  

Les deux dictionnaires permettent la construction du dashboard, quels boutons, quels graphiques, quels titres etc.

Sur cette exemple nous aurons donc quatres graphiques, le titre du dashboard (namedb) un bouton accueil, et trois sous titre.

  

Ils sont initialisés par le wrapper DigiDashBoard

  

![enter image description here](https://i.ibb.co/Zhs8M8L/screen2-page-utilisateur.png)

  

Ensuite, ils sont utilisés par Flask pour Jinja2

  

![enter image description here](https://i.ibb.co/R9F7VVk/screen3-page-utilisateur.png)

  

Les pages HTML, CSS, JS sont déjà prêtes, il suffit simplement de renseigner les boutons, titre et sous titre et les graphiques dans les dictionnaires pour lancer l'application.

  

## Lancement de l'application

  

Pour lancer l'application, il vous suffit de télécharger le projet depuis le gitlab.

  

Installez les technologies mentionnées au préalable, puis lancer Utilisateur.py.

  

Pour le moment, seul les graphiques déjà écrits dans le dictionnaire "graphiques" sont disponibles.

  

L'application est pour le moment essentiellement en local.

  

Voici l'interface de l'application avec le projet nutritionnelle

  

![enter image description here](https://i.ibb.co/m955gLJ/Screen-dashboard-nutrition.png)

  

Vous avez sur la gauche la liste des graphiques, et d'autres boutons, le seul fonctionnel en dehors des graphique et de l'accueil est le bouton recommandation

  

![enter image description here](https://i.ibb.co/4m0wLQJ/Screen-dashboard-nutrition2.png)

  

Cette fonctionnalité propose les 5 fruits/légumes ayant la plus haute teneur en la valeur nutritionnelle renseignée dans la liste.

  

## Structure des fichiers

  

La structure des fichiers est actuellements construite de cette manière:

    ├── fichiers excel
    │   ├── fruits_legumes
    │   │   ├── Abricot.xlsx
    │   │   ├── Acérola.xlsx
    │   │   ├── Airelles.xlsx
    │   │   ├── Ananas.xlsx
    │   │   ... 
    │   ├── tableau_des_aliments.xlsx
    │   ├── tableau_des_aliments2.xlsx
    │   ├── tableau_des_aliments3.xlsx
    │   │   ... 
    ├── json
    │   ├── constructeur_json.py
    │   ├── courbes
    │   │   └── courbe_defaut.json
    │   ├── dcirculaires
    │   │   ├── dcirculaire.json
    │   │   └── dcirculaire_defaut.json
    │   ├── histogrammes
    │   │   ├── histogramme.json
    │   │   └── histogramme_defaut.json
    │   ├── sliced
    │   │   ├── sliced.json
    │   │   └── sliced_defaut.json
    │   ├── sliced_micro
    │   │   └── sliced_micro.json
    │   ├── tables
    │   │   ├── table.json
    │   │   └── table_defaut.json
    │   └── test_constructeur_json.py
    ├── Scrapping
    │   ├── chromedriver.exe
    │   ├── debug.log
    │   ├── Scrapping avec try except.py
    │   └── Scrapping_et_trie.py
    ├── static
    │   ├── css
    │   │   ├── courbe.css
    │   │   ├── diagramme_circulaire.css
    │   │   ├── histogramme.css
    │   │   ├── sliced.css
    │   │   ├── style.css
    │   │   ├── table.css
    │   │   └── tableinteractive.css
    │   ├── images
    │   │   ├── background2.png
    │   │   ├── sort_asc.png
    │   │   ├── sort_asc_disabled.png
    │   │   ├── sort_both.png
    │   │   ├── sort_desc.png
    │   │   └── sort_desc_disabled.png
    │   └── js
    │       ├── courbe.js
    │       ├── diagrammes_circulaires.js
    │       ├── histogramme.js
    │       ├── sliced.js
    │       ├── sliced_micro.js
    │       ├── table.js
    │       └── tableinteractive.js
    ├── templates
    │   ├── accueil.html
    │   ├── courbe.html
    │   ├── diagrammes_circulaires.html
    │   ├── histogramme.html
    │   ├── index.html
    │   ├── recommandation.html
    │   ├── sliced.html
    │   ├── sliced_micro.html
    │   ├── Table.html
    │   └── Tableinteractive.html
    ├── DigiDashBoard.py
    ├── Flaskdigidashboard.py
    ├── fonction recette.py
    ├── fonctions_carences.py
    ├── geckodriver.log
    ├── LICENSE.md
    ├── README.md
    └── Utilisateur.py

## Base de données

  

Pour le moment, les données sont récupérées depuis les fichiers Excels générés par Scrapping_et_trie.py.

Les données utilisées pour la construction des json pour les graphiques proviennent du fichier excel tableau_des_aliments6.xlsx, elles sont
également utilisées pour la fonctionnalité des carences et des recettes.


Il y a malgré tout la possibilité de créer la tables à l'aide de Sqlalchemy (dans le projet, la table est renvoyé sur PostgreSQL en local).

  

## License

  

MIT

