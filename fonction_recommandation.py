# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 10:26:34 2020

L'utilisateur peut utiliser l'application pour avoir une proposition
alimentaire en fonction de ses besoins.

Si il renseigne le besoin de consommer du fer, l'application doit lui proposer
une liste de quelques aliments contenant le plus de fer parmis les autres
fruits et légumes.

@author: Reda
"""

import pandas as pd

tablealiments = pd.read_excel('tableau_des_aliments6.xlsx', index_col=0)


# =============================================================================
#                               TEST MAX DATAFRAME
# =============================================================================

# tablealiments.max(level=0)

# tablealiments['Eau g'].max(axis = 0)

# tablealiments.max()

# tablealiments.loc[tablealiments['Eau g'].idxmax()]

# tablealiments.loc[tablealiments.loc['Abricot'].argmax()]

# max_valeur = tablealiments_alimentmax.iloc[0][valeur_nutritionnelle]

# max_row = tablealiments.loc['Abricot'].values.argmax()

# =============================================================================
#                         FONCTION DE RECOMMANDATIONS
# =============================================================================


# Fonction qui renvoie l'aliment ayant la valeur nutritionnelle resenseignée
# la plus haute.
def recommandation_aliment_1(valeur_nutritionnelle):
    # Donne la ligne comportant la valeur maximal de la colonne.
    tablealiments_alimentmax = (
        tablealiments.loc[tablealiments[valeur_nutritionnelle]
                          == tablealiments[valeur_nutritionnelle].max()])

    # Donne l'aliment où se trouve la valeur maximal de la colonne.
    aliment_recommande = tablealiments_alimentmax.index[0]

    # Retourne l'aliment en fonction de la valeur nutritionelle renseignée.
    return aliment_recommande


# Fonction qui renvoie les 5 aliments ayant la valeur nutritionnelle
# resenseignée la plus haute.
def recommandation_aliment_5(valeur_nutritionnelle):
    # Contient les 5 aliments ayant la valeur nutritionnelle resenseignée la
    # plus haute.
    liste_aliments_max = []

    # Change l'ordre des lignes de manière decroissante, les lignes ayant les
    # valeurs les plus hautes de la valeur nutritionnelle renseignée se trouve
    # en tête de la dataframe.
    aliments_max_decroissant = (tablealiments.sort_values
                                ([valeur_nutritionnelle], ascending=False)
                                .groupby(valeur_nutritionnelle).head(1))

    # Place les 5 premieres lignes de la dataframe dans la variable
    # 'aliments_max_5'.
    aliments_max_5 = aliments_max_decroissant[valeur_nutritionnelle].head(5)

    # Itère sur les index de la variable aliments_max_5 (les aliments), puis
    # ajoute le nom des index dans la liste 'liste_aliments_max', le nom des
    # index sont en fait les aliments.
    for aliment in aliments_max_5.index:
        liste_aliments_max.append(aliment)

    # Retourne la liste en fonction de la valeur nutritionelle renseignée.
    return liste_aliments_max
